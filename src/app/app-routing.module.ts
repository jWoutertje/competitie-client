import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompetitieNewComponent } from './components/competitie-new/competitie-new.component';
import { CompetitieListComponent } from './components/competitie-list/competitie-list.component';
import { CompetitieDetailComponent } from './components/competitie-detail/competitie-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/competities', pathMatch: 'full' },
  { path: 'competities/new', component: CompetitieNewComponent },
  { path: 'competities/:competitieId/detail', component: CompetitieDetailComponent },
  { path: 'competities', component: CompetitieListComponent },
  { path: '**', redirectTo: '/competities' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
