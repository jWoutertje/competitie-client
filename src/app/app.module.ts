import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';

import { CompetitieService } from './services/competitie.service';

import { AppComponent } from './components/app.component';
import { CompetitieNewComponent } from './components/competitie-new/competitie-new.component';
import { CompetitieListComponent } from './components/competitie-list/competitie-list.component';
import { CompetitieDetailComponent } from './components/competitie-detail/competitie-detail.component';
import { WedstrijdEditComponent } from './components/wedstrijd-edit/wedstrijd-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    CompetitieNewComponent,
    CompetitieListComponent,
    CompetitieDetailComponent,
    WedstrijdEditComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  entryComponents: [
    WedstrijdEditComponent,
  ],
  providers: [
    CompetitieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
