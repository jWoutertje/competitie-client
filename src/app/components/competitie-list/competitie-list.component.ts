import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CompetitieService} from '../../services/competitie.service';
import {Competitie} from '../../models/competitie';

@Component({
  selector: 'app-competitie-list',
  templateUrl: './competitie-list.component.html',
  styleUrls: ['./competitie-list.component.css']
})
export class CompetitieListComponent implements OnInit {
  competities: Competitie[];

  constructor(
    private router: Router,
    private competitieService: CompetitieService
  ) {}

  ngOnInit() {
    this.getCompetities();
  }

  getCompetities(): void {
    this.competitieService
      .getCompetities()
      .subscribe(
        competities => {
          this.competities = competities;
          this.getStatussen();
        }
      );
  }

  getStatussen() {
    this.competities.forEach(competitie => {
      let aantal = 0;
      competitie.wedstrijden.forEach( wedstrijd => {
        if (wedstrijd.datum) {
          aantal++;
        }
      });
      competitie.percentage = Math.round(aantal / competitie.wedstrijden.length * 100);
    });
  }

  goToNew(): void {
    this.router.navigate(['/competities/new']);
  }

  gotoDetail(competitie: Competitie): void {
    this.router.navigate(['/competities', competitie.id, 'detail']);
  }
}

