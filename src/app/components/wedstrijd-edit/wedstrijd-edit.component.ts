import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-wedstrijd-edit',
  templateUrl: './wedstrijd-edit.component.html',
  styleUrls: ['./wedstrijd-edit.component.css']
})
export class WedstrijdEditComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<WedstrijdEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any ) { }

  ngOnInit() {}

  close() {
    this.dialogRef.close(null);
  }

  save() {
    this.dialogRef.close(this.data.wedstrijd);
  }
}
