import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CompetitieService} from '../../services/competitie.service';
import {Competitie} from '../../models/competitie';
import {MatDialog} from '@angular/material';
import {WedstrijdEditComponent} from '../wedstrijd-edit/wedstrijd-edit.component';
import {Wedstrijd} from '../../models/wedstrijd';

@Component({
  selector: 'app-competitie-details',
  templateUrl: './competitie-detail.component.html',
  styleUrls: ['./competitie-detail.component.css']
})
export class CompetitieDetailComponent implements OnInit {
  competitie: Competitie;
  activeTab = 0;
  displayedColumnsWedstrijden: string[] = ['thuis', 'tegen', 'uit', 'aanpassen'];
  displayedColumnsUitslagen: string[] = ['datum', 'thuis', 'uitslag', 'uit', 'end2'];
  displayedColumnsSpelers: string[] = ['id', 'naam', 'gespeeld', 'doelsaldo', 'gewonnen', 'end2'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private competitieService: CompetitieService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.getCompetitie(0);
  }

  sortDeelnemers<T>(): void {
      this.competitie.deelnemers.sort((a, b) => {
        if (a['gewonnen'] < b['gewonnen']) {
          return -1;
        }
        if (a['gewonnen'] > b['gewonnen']) {
          return 1;
        }
        if (a['doelsaldo'] < b['doelsaldo']) {
          return -1;
        }
        if (a['doelsaldo'] > b['doelsaldo']) {
          return 1;
        }
        return 0;
      });
    this.competitie.deelnemers.reverse();
  }

  sortWedstrijden<T>(): void {
    this.competitie.wedstrijden.sort((a, b) => {
      if (a.datum != null && b.datum != null) {
        const aa =
          a.datum.substr(6, 4) +
          a.datum.substr(3, 2) +
          a.datum.substr(1, 2) +
          a.datum.substr(11, 2) +
          a.datum.substr(13, 2) +
          a.datum.substr(15, 2);
        const bb =
          b.datum.substr(6, 4) +
          b.datum.substr(3, 2) +
          b.datum.substr(1, 2) +
          b.datum.substr(11, 2) +
          b.datum.substr(13, 2) +
          b.datum.substr(15, 2);
        return aa < bb ? -1 : (aa > bb ? 1 : 0);
      }
    });
    this.competitie.wedstrijden.reverse();
  }

  getCompetitie(tab: number): void {
    const id = +this.route.snapshot.paramMap.get('competitieId');
    this.competitieService
      .getCompetitie(id)
      .subscribe(competitie => {
          this.competitie = competitie;
          this.sortDeelnemers();
          this.sortWedstrijden();
          this.activeTab = tab;
      });
  }

  getProgramma(): Wedstrijd[] {
    const wedstrijden = [];
    this.competitie.wedstrijden.forEach(wedstrijd => {
      if (!wedstrijd.datum) {
        wedstrijden.push(wedstrijd);
      }
    })
    return wedstrijden;
  }

  getUitslagen(): Wedstrijd[] {
    const wedstrijden = [];
    this.competitie.wedstrijden.forEach(wedstrijd => {
      if (wedstrijd.datum) {
        wedstrijden.push(wedstrijd);
      }
    })
    return wedstrijden;
  }

  deleteCompetitie(): void {
    this.competitieService
      .deleteCompetitie(this.competitie.id)
      .subscribe(
        res => { this.router.navigate(['/competities']); }
      );
  }

  goBack(): void {
    this.router.navigate(['/competities']);
  }

  popupAddWedstrijd(wedstrijd: Wedstrijd): void {
    const dialogRef = this.dialog.open(WedstrijdEditComponent, {
      data: { wedstrijd: wedstrijd, deelnemers: this.competitie.deelnemers }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const datum = new Date();
        result.datum = datum.toISOString();
        this.competitieService
          .putWedstrijd(this.competitie.id, result)
          .subscribe(w => {
            this.getCompetitie(1);
          });
      } else {
        this.getCompetitie(0);
      }
    });
  }
}
