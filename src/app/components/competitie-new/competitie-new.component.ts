import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CompetitieService} from '../../services/competitie.service';
import {Competitie} from '../../models/competitie';
import {Deelnemer} from '../../models/deelnemer';

@Component({
  selector: 'app-competitie-new',
  templateUrl: './competitie-new.component.html',
  styleUrls: ['./competitie-new.component.css']
})
export class CompetitieNewComponent implements OnInit {
  teamgroottes: number[] = [1, 2];
  competitietypes: string[] = ['Vaste teams', 'Variabele teams'];
  competitie: Competitie;

  constructor(
    private router: Router,
    private competitieService: CompetitieService
  ) {}

  ngOnInit() {
    this.competitie = new Competitie();
    this.newDeelnemers(2);
  }

  addDeelnemer(deelnemerNummer: Number): void {
    if (deelnemerNummer === this.competitie.deelnemers.length) {
      this.competitie.deelnemers.push(new Deelnemer());
    }
  }

  newDeelnemers(aantal: number): void {
    for (let i = 1; i <= aantal; i++) {
      this.competitie.deelnemers.push(new Deelnemer());
    }
  }

  save(): void {
    if (this.competitie.teamgrootte === 2 && this.competitie.competitietype === this.competitietypes[0]) {
      this.competitie.deelnemers.forEach(deelnemer => {
        deelnemer.naam = deelnemer.naam + ', ' + deelnemer.naam2;
        deelnemer.naam2 = null;
      });
    }
    if (this.competitie.teamgrootte === 1) {
      this.competitie.competitietype = this.competitietypes[0];
    }


    // console.log(JSON.stringify(this.competitie));
    this.competitieService
      .postCompetitie(this.competitie)
      .subscribe(
        competitie => {
          this.competitie = competitie;
          this.router.navigate(['/competities', competitie.id]);
        }
      );
  }

  goBack(): void {
    this.router.navigate(['/competities']);
  }
}
