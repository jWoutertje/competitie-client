import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Competitie } from '../models/competitie';
import { Wedstrijd } from '../models/wedstrijd';
import { Deelnemer } from '../models/deelnemer';

import { environment } from '../../environments/environment';


@Injectable()
export class CompetitieService {
  private competitieUrl = environment.apiUrl + '/competities';

  constructor(
    private http: HttpClient
  ) {}

  getCompetities() {
    return this.http
      .get<Competitie[]>(this.competitieUrl)
      .pipe(map(data => data), catchError(this.handleError));
  }

  getCompetitie(competitieId: number) {
    const url = `${this.competitieUrl}/${competitieId}`;
    return this.http
      .get<Competitie>(url)
      .pipe(map(data => data), catchError(this.handleError));
  }

  postCompetitie(competitie: Competitie) {
    return this.http
      .post<Competitie>(this.competitieUrl, competitie)
      .pipe(catchError(this.handleError));
  }

  deleteCompetitie(competitieId: number) {
    const url = `${this.competitieUrl}/${competitieId}`;
    return this.http
      .delete<void>(url)
      .pipe(catchError(this.handleError));
  }

  putWedstrijd(competitieId: number, wedstrijd: Wedstrijd) {
    const url = `${this.competitieUrl}/${competitieId}/wedstrijden/${wedstrijd.id}`;
    return this.http
      .put<void>(url, wedstrijd)
      .pipe(catchError(this.handleError));
  }

  private handleError(res: HttpErrorResponse | any) {
    console.error(res.error || res.body.error);
    return observableThrowError(res.error || 'Server error');
  }
}
