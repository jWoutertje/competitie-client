export class Deelnemer {

  id: number;
  naam: string;
  gespeeld: number;
  doelsaldo: number;
  gewonnen: number;

  // alleen voor client
  naam2: string;

  constructor() {}
}
