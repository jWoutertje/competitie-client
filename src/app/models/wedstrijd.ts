import {Speler} from './speler';

export class Wedstrijd {

  id: number;
  datum: string;
  spelers: Speler[];
  scoreThuis: number;
  scoreUit: number;

  constructor() {
    this.spelers = [];
  }
}
