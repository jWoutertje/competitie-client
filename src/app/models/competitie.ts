import {Wedstrijd} from './wedstrijd';
import {Deelnemer} from './deelnemer';

export class Competitie {

  id: number;
  naam: string;
  competitietype: string;
  teamgrootte: number;
  wedstrijden: Wedstrijd[];
  deelnemers: Deelnemer[];

  // alleen voor client
  percentage: number;

  constructor() {
    this.wedstrijden = [];
    this.deelnemers = [];
  }
}
